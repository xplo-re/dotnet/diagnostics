﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using JetBrains.Annotations;


namespace XploRe.Diagnostics
{

    /// <summary>
    ///     Command-line utilities.
    /// </summary>
    public static class ProcessUtils
    {

        /// <summary>
        ///     Platform-dependent escaped version of an empty argument.
        /// </summary>
        [NotNull]
        public static string EscapedEmptyArgument => "\"\"";

        /// <summary>
        ///     Escapes arguments for spawning processes or passing arguments as a single argument.
        /// </summary>
        /// <param name="args">Collection of arguments to escape.</param>
        /// <returns>Escaped command-line string.</returns>
        [NotNull]
        public static string EscapeArguments(IEnumerable<string> args)
        {
            if (args == null) {
                return string.Empty;
            }

            Func<string, string> escape;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                escape = EscapeArgumentForWindows;
            }
            else {
                escape = EscapeArgumentForUnix;
            }

            // Each argument is wrapped in quotation marks if necessary.
            var result = string.Join(" ", args.Select(escape));
            Debug.Assert(result != null, $"{nameof(result)} != null");

            return result;
        }

        [NotNull]
        internal static string EscapeArgumentForWindows([CanBeNull] string arg)
        {
            if (string.IsNullOrEmpty(arg)) {
                return EscapedEmptyArgument;
            }

            // On Windows, backslash escapes are only parsed if they precede quotation marks and only cover backslashes
            // and quotation marks.
            // https://msdn.microsoft.com/en-us/library/system.environment.getcommandlineargs.aspx
            // https://blogs.msdn.microsoft.com/twistylittlepassagesallalike/2011/04/23/everyone-quotes-command-line-arguments-the-wrong-way/
            // https://stackoverflow.com/questions/5510343/escape-command-line-arguments-in-c-sharp

            // Double backslashes that precede a quotation mark.
            var escaped = Regex.Replace(arg, @"(\\*)" + "\"", @"$1\$0");
            // Wrap all lines into quotation marks that contain whitespaces. Double backslashes at end of line.
            escaped = Regex.Replace(escaped, @"^(.*\s.*?)(\\*)$", "\"$1$2$2\"", RegexOptions.Singleline);

            return escaped ?? string.Empty;
        }

        [NotNull]
        internal static string EscapeArgumentForUnix([CanBeNull] string arg)
        {
            if (string.IsNullOrEmpty(arg)) {
                return EscapedEmptyArgument;
            }

            // If used as arguments for a Process, .NET Core unparses the command-line arguments by itself after
            // arguments were escaped for ProcessStartInfo.
            // Use a mechanism similar to the .NET Core version at
            // https://github.com/dotnet/corefx/blob/master/src/System.Runtime.Extensions/src/System/Environment.cs#L64
            var requiresQuotes = false;
            var requiresEscape = false;
            var result = arg;
            const string quote = "\"";

            foreach (var ch in arg) {
                if (char.IsWhiteSpace(ch) || ch == '\'') {
                    requiresQuotes = true;
                }
                else if (ch == '"') {
                    requiresEscape = true;
                }
            }

            if (requiresEscape) {
                result = result.Replace(quote, "\\\"");
            }

            if (requiresQuotes) {
                result = quote + result + quote;
            }

            return result ?? string.Empty;
        }

    }

}
