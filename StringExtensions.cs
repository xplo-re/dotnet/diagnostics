﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Text;
using JetBrains.Annotations;


namespace XploRe.Diagnostics
{

    /// <summary>
    /// Provides diagnostic helper extensions for <see cref="string" /> objects.
    /// </summary>
    public static class StringExtensions
    {

        /// <summary>
        /// Appends a sentence to the current string for diagnostic messages.
        /// </summary>
        /// <param name="self">The current string to append <paramref name="sentence" /> to.</param>
        /// <param name="sentence">The string to append to the current string with sentence separators..</param>
        /// <returns>
        /// Formatted diagnostic message. A full stop and whitespace is added as needed to separate the original message
        /// from the appended sentence; additional whitespaces are removed from the end of the current string, unless it
        /// ends in a newline. If the current string is <c>null</c>, only <paramref name="sentence" /> is returned. 
        /// <paramref name="sentence" /> is always trimmed at the start. If <paramref name="sentence" /> is <c>null</c>,
        /// the current string is returned unchanged. If both strings are <c>null</c>, <c>null</c> is returned.
        /// </returns>
        [MustUseReturnValue]
        [CanBeNull]
        [ContractAnnotation("self:null,sentence:null => null")]
        public static string AppendDiagnosticsSentence([CanBeNull] this string self, [CanBeNull] string sentence)
        {
            if (self == null) {
                // If current string is null, the result is the string to be appended.
                return sentence?.TrimStart();
            }

            if (sentence == null) {
                return self;
            }

            var sb = new StringBuilder();

            if (self.EndsWith(Environment.NewLine, StringComparison.Ordinal)) {
                sb.Append(self);
            }
            else {
                var trimmed = self.TrimEnd();

                sb.Append(trimmed);

                if (trimmed?.EndsWith(".") == false) {
                    sb.Append(".");
                }

                sb.Append(" ");
            }

            sb.Append(sentence.TrimStart());

            return sb.ToString();
        }

    }

}
